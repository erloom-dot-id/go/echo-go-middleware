package echogomiddleware

import (
	"context"
	"fmt"
	"net/http"
	"time"

	echogologger "gitlab.com/erloom-dot-id/go/echo-go-logger"
	"gitlab.com/erloom-dot-id/go/echo-go-logger/elk"
	sentryecho "gitlab.com/erloom-dot-id/go/echo-go-logger/sentry"
	"go.uber.org/zap"
)

func TraceOtherService(ctx context.Context, tran, op, service_type string) context.Context {
	span := sentryecho.StartTransaction(ctx, tran, op)
	defer span.Finish()

	reqID := span.TraceID.String()

	ctx = context.WithValue(ctx, echogologger.CtxRequestID, reqID)

	status := http.StatusOK

	span.Status = httpSpanStatusCode[status]

	startTime := time.Now()
	elapsed := time.Since(startTime)

	elk.LogInfo(ctx, "service completed to running", []zap.Field{
		zap.Int("status", status),
		zap.String("duration", fmt.Sprintf("%d ms", elapsed.Milliseconds())),
		zap.String("type", service_type),
	}...)

	return ctx
}
