# Echo Go-Middleware

Package Echo Go-Middleware digunakan untuk mempermudah developer dalam membuat endpoint. Dengan package ini, developer tidak perlu membuat middleware dari nol, karena package ini sudah menyediakan beberapa middleware yang dapat digunakan untuk keperluan endpoint. Package ini juga menggunakan package [echo-go-logger](https://gitlab.com/erloom-dot-id/go/echo-go-logger) dalam menyimpan log dan juga tracing.

## Installation

Jalankan perintah berikut untuk meng-install package ini

```bash
go get gitlab.com/erloom-dot-id/go/echo-go-middleware@latest
```

## Usage

Misal project kalian menggunakan package router [go-chi](https://github.com/go-chi/chi)

```go
import (
    "github.com/go-chi/chi/v5"
	echogomiddleware "gitlab.com/erloom-dot-id/go/echo-go-middleware"
)

func main() {
    
    router := chi.NewRouter()

    router.Route("/api", func(r chi.Router) {

        // StandardMiddleware untuk mencatat log endpoint yang di hit. Isi dari 
        // log tersebut ada http status dan juga waktu proses callback dari endpoint.
        r.Use(echogomiddleware.StandardMiddleware)

        // PanicMiddleware berfungsi untuk menangkap log error ketika 
        // endpoint yg dipanggil meng-callback response error
		r.Use(echogomiddleware.PanicMiddleware)
        
		r.Get("/my-info", function.GetMyInfo)
	})
}
```

Jika di project kalian diharuskan untuk menyimpan semua log dan juga menghitung trace dari semua endpoint yang dibuat, kalian bisa meng-init sentry dan memanggil PanicMiddleware dan juga InstrumentMiddleware.

```go
import (
    "github.com/getsentry/sentry-go"
    "github.com/go-chi/chi/v5"
	echogomiddleware "gitlab.com/erloom-dot-id/go/echo-go-middleware"
)

func init() {

    err := sentry.Init(sentry.ClientOptions{
        Dsn:                "Dsn project yang ada di sentry",
        EnableTracing:      true,
        TracesSampleRate:   0.5,
    })
}

func main() {

    router := chi.NewRouter()

    router.Route("/api", func(r chi.Router) {

        // InstrumentMiddleware digunakan untuk me-tracing endpoint yang 
        // ada di dalam middleware ini dan akan disimpan di dalam sentry.
        r.Use(echogomiddleware.InstrumentMiddleware)

        // PanicMiddleware dapat menyimpan sesuatu respon error dan 
        // disimpan di dalam sentry.
		r.Use(echogomiddleware.PanicMiddleware)

		r.Get("/my-info", function.GetMyInfo)
	})
}
```

Dengan menambahkan InstrumentMiddleware, di sentry kalian akan menampilkan performance dari semua endpoint yang kalian buat. Jika di dalam endpoint kalian terdapat sebuat proses query, contohnya query ke database kalian, kalian bisa me-tracing query tersebut. Tracing query database akan muncul jika fungsi query kamu ditambahkan StartSpan. Fungsi StartSpan bisa kamu dapat dari package [echo-go-logger](https://gitlab.com/erloom-dot-id/go/echo-go-logger). Misal kalian memiliki dua file, yaitu file usecase.go untuk memproses bisnis logic kamu dan file repository.go untuk fungsi query ke database kamu.

file usecase.go
```go
import (
    "context"

	sentryecho "gitlab.com/erloom-dot-id/go/echo-go-logger"
)

type User struct {
    ID      int     `json:"id"`
    Name    string  `json:"name"`
}

func GetUser(ctx context.Context) ([]User, error) {

    // Input "usecase.GetUser" adalah nama span yang akan ditampilkan di sentry
    ctx, span := sentryecho.StartSpan(ctx, "usecase.GetUser")
	defer span.Finish()

    users, err := QueryGetUser(ctx)
    if err != nil {
        return users, err
    }

    return users, nil
}
```

file repository.go
```go
import (
	"context"
	"gorm.io/gorm"

	sentryecho "gitlab.com/erloom-dot-id/go/echo-go-logger/sentry"
)

func QueryGetUser(ctx context.Context) ([]User, error) {

    // Input "repository.GetUser" adalah nama span yang akan ditampilkan di sentry
    ctx, span := sentryecho.StartSpan(ctx, "repository.GetUser")
	defer span.Finish()

    var users []User

    err := db.Find(&users).Error()
    if err != nil {
        return users, err
    }

    return users, nil
}
```

Jika kalian memiliki sebuah service dimana service tersebut bukanlah service api, kalian bisa menambahkan function TraceOtherService untuk me-tracing script dan juga CapturePanicError untuk menangkap error.

```go
import (
	"context"

	echogomiddleware "gitlab.com/erloom-dot-id/go/echo-go-middleware"
)

func RunMyService(ctx context.Context) {
	span := sentryecho.StartTransaction(ctx, "[cron] RunMyService", "http.server")
    defer span.Finish()

    CapturePanicError(ctx)
}
```