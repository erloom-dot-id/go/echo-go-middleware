module gitlab.com/erloom-dot-id/go/echo-go-middleware

go 1.20

require (
	github.com/felixge/httpsnoop v1.0.3
	github.com/getsentry/sentry-go v0.18.0
	github.com/go-playground/universal-translator v0.18.1
	github.com/go-playground/validator/v10 v10.11.2
	github.com/google/uuid v1.3.0
	gitlab.com/erloom-dot-id/go/echo-go-logger v1.4.1
	go.uber.org/zap v1.27.0
)

require (
	github.com/go-playground/locales v0.14.1 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/crypto v0.5.0 // indirect
	golang.org/x/sys v0.4.0 // indirect
	golang.org/x/text v0.6.0 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.2.1 // indirect
)
