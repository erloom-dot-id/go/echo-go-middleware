package echogomiddleware

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	"github.com/felixge/httpsnoop"
	"github.com/getsentry/sentry-go"
	"github.com/google/uuid"
	echogologger "gitlab.com/erloom-dot-id/go/echo-go-logger"
	"gitlab.com/erloom-dot-id/go/echo-go-logger/elk"
	sentryecho "gitlab.com/erloom-dot-id/go/echo-go-logger/sentry"
	"go.uber.org/zap"
)

func PanicMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		defer func() {
			if r := recover(); r != nil {

				// Recover with sentry
				sentryecho.Recover(request.Context(), r)

				var code int
				var httpErrors []HttpResponseError
				var errorMsg string
				switch err := r.(type) {
				case error:
					code, httpErrors = InternalServerError{Message: err.Error()}.ToHttpError()
					errorMsg = err.Error()
				default:
					code, httpErrors = InternalServerError{Message: fmt.Sprintf("unkown error: %v", err)}.ToHttpError()
					errorMsg = fmt.Sprintf("unkown error: %v", err)
				}

				// Log Error witk ELK
				elk.LogError(request.Context(), errorMsg, []zap.Field{
					zap.String("tag", "k8s-log"),
				}...)

				meta := MetaResponse{
					HTTPCode: code,
				}
				res := Response{
					Errors: httpErrors,
					Meta: MetaResponse{
						HTTPCode: code,
					},
				}

				writer.WriteHeader(meta.HTTPCode)
				responseBody, _ := json.Marshal(res)
				_, _ = writer.Write(responseBody)
			}
		}()

		next.ServeHTTP(writer, request)
	})
}

var httpSpanStatusCode = map[int]sentry.SpanStatus{
	http.StatusOK:                  sentry.SpanStatusOK,
	http.StatusCreated:             sentry.SpanStatusOK,
	http.StatusAccepted:            sentry.SpanStatusOK,
	http.StatusUnauthorized:        sentry.SpanStatusUnauthenticated,
	http.StatusForbidden:           sentry.SpanStatusPermissionDenied,
	http.StatusNotFound:            sentry.SpanStatusNotFound,
	http.StatusBadRequest:          sentry.SpanStatusInvalidArgument,
	http.StatusInternalServerError: sentry.SpanStatusInternalError,
	http.StatusBadGateway:          sentry.SpanStatusInternalError,
}

type statusRecorder struct {
	http.ResponseWriter
	status int
}

func InstrumentMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		span := sentryecho.StartTransaction(request.Context(), fmt.Sprintf("[%s] %s", request.Method, request.URL.Path), "http.server")
		defer span.Finish()
		ctx := span.Context()
		startTime := time.Now()
		reqID := request.Header.Get(string(echogologger.CtxRequestID))
		if reqID == "" {
			reqID = span.TraceID.String()
		}

		ctx = context.WithValue(ctx, echogologger.CtxRequestID, reqID)

		w := &statusRecorder{
			ResponseWriter: writer,
			status:         http.StatusOK,
		}
		next.ServeHTTP(w, request.WithContext(ctx))

		elapsed := time.Since(startTime)

		elk.LogInfo(ctx, "http handler completed", []zap.Field{
			zap.String("duration", fmt.Sprintf("%d ms", elapsed.Milliseconds())),
			zap.String("method", request.Method),
			zap.String("path", request.URL.Path),
		}...)
		span.Status = httpSpanStatusCode[w.status]
	})
}

func StandardMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		startTime := time.Now()
		reqID := request.Header.Get(string(echogologger.CtxRequestID))
		if reqID == "" {
			reqID = uuid.NewString()
		}

		ctx := context.WithValue(request.Context(), echogologger.CtxRequestID, reqID)

		w := &statusRecorder{
			ResponseWriter: writer,
			status:         http.StatusOK,
		}
		next.ServeHTTP(w, request.WithContext(ctx))

		elapsed := time.Since(startTime)

		elk.LogInfo(ctx, "Http handler completed", []zap.Field{
			zap.Int("status", w.status),
			zap.String("duration", fmt.Sprintf("%d ms", elapsed.Milliseconds())),
		}...)
	})
}

func InstrumentMiddlewareV1(next http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		var reqBodyJson string
		var reqBodyForm string
		if request.Method != http.MethodGet {
			var body, _ = io.ReadAll(request.Body)
			request.Body = io.NopCloser(bytes.NewBuffer(body))
			reqBody := string(body)

			reqBodyIsForm := strings.Contains(reqBody, "=")
			if reqBodyIsForm {
				const maxUploadSize = 20 * 1024 * 1024
				err := request.ParseMultipartForm(maxUploadSize)
				if err == nil {
					var reqBodyFormMap = map[string]interface{}{}
					for key, values := range request.MultipartForm.Value {
						for _, value := range values {
							reqBodyFormMap[key] = value
						}
					}
					reqBodyFormJson, _ := json.Marshal(reqBodyFormMap)

					reqBodyForm = string(reqBodyFormJson)
				}
			} else {
				reqBodyJson = reqBody
			}
		}

		span := sentryecho.StartTransaction(request.Context(), fmt.Sprintf("[%s] %s", request.Method, request.URL.Path), "http.server")
		defer span.Finish()
		ctx := span.Context()
		reqID := request.Header.Get(string(echogologger.CtxRequestID))
		if reqID == "" {
			reqID = span.TraceID.String()
		}

		ctx = context.WithValue(ctx, echogologger.CtxRequestID, reqID)

		m := httpsnoop.CaptureMetrics(PanicMiddleware(next), writer, request.WithContext(ctx))

		span.Status = httpSpanStatusCode[m.Code]

		elk.LogInfo(ctx, fmt.Sprintf("http handler ([%s] - %s) completed", request.Method, request.URL.Path), []zap.Field{
			zap.Int("status", m.Code),
			zap.String("duration", fmt.Sprintf("%d ms", m.Duration.Milliseconds())),
			zap.String("method", request.Method),
			zap.String("path", request.URL.Path),
		}...)

		elk.TrafficLogInfo(ctx, fmt.Sprintf("Traffic log: [%s] - %s", request.Method, request.URL.Path), []zap.Field{
			zap.String("path", request.URL.Path),
			zap.String("host", request.Host),
			zap.String("method", request.Method),
			zap.Any("user_agent", request.UserAgent()),
			zap.String("request_body", reqBodyJson),
			zap.String("request_form", reqBodyForm),
			zap.Int("response_status", m.Code),
		}...)
	})
}

func StandardMiddlewareV1(next http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		var reqBodyJson string
		var reqBodyForm string
		if request.Method != http.MethodGet {
			var body, _ = io.ReadAll(request.Body)
			request.Body = io.NopCloser(bytes.NewBuffer(body))
			reqBody := string(body)

			reqBodyIsForm := strings.Contains(reqBody, "=")
			if reqBodyIsForm {
				const maxUploadSize = 20 * 1024 * 1024
				err := request.ParseMultipartForm(maxUploadSize)
				if err == nil {
					var reqBodyFormMap = map[string]interface{}{}
					for key, values := range request.MultipartForm.Value {
						for _, value := range values {
							reqBodyFormMap[key] = value
						}
					}
					reqBodyFormJson, _ := json.Marshal(reqBodyFormMap)

					reqBodyForm = string(reqBodyFormJson)
				}
			} else {
				reqBodyJson = reqBody
			}
		}

		reqID := request.Header.Get(string(echogologger.CtxRequestID))
		if reqID == "" {
			reqID = uuid.NewString()
		}

		ctx := context.WithValue(request.Context(), echogologger.CtxRequestID, reqID)

		m := httpsnoop.CaptureMetrics(PanicMiddleware(next), writer, request.WithContext(ctx))

		elk.LogInfo(ctx, fmt.Sprintf("http handler ([%s] - %s) completed", request.Method, request.URL.Path), []zap.Field{
			zap.Int("status", m.Code),
			zap.String("duration", fmt.Sprintf("%d ms", m.Duration.Milliseconds())),
			zap.String("method", request.Method),
			zap.String("path", request.URL.Path),
		}...)

		elk.TrafficLogInfo(ctx, fmt.Sprintf("Traffic log: [%s] - %s", request.Method, request.URL.Path), []zap.Field{
			zap.String("path", request.URL.Path),
			zap.String("host", request.Host),
			zap.String("method", request.Method),
			zap.Any("user_agent", request.UserAgent()),
			zap.String("request_body", reqBodyJson),
			zap.String("request_form", reqBodyForm),
			zap.Int("response_status", m.Code),
		}...)
	})
}
