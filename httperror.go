package echogomiddleware

import (
	"context"
	"fmt"

	"gitlab.com/erloom-dot-id/go/echo-go-logger/elk"
	sentryecho "gitlab.com/erloom-dot-id/go/echo-go-logger/sentry"
)

func CapturePanicError(ctx context.Context) {
	defer func() {
		if r := recover(); r != nil {

			// Capture something went wrong and logging on sentry
			sentryecho.Recover(ctx, r)

			var errorMsg string
			switch err := r.(type) {
			case error:
				errorMsg = err.Error()
			default:
				errorMsg = fmt.Sprintf("unkown error: %v", err)
			}

			// Log Error witk ELK
			elk.LogError(ctx, errorMsg)
		}
	}()
}
