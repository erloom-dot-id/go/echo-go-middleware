package echogomiddleware

import "net/http"

type HttpError interface {
	ToHttpError() (int, []HttpResponseError)
}

type HttpResponseError struct {
	Field   string `json:"field"`
	Message string `json:"message"`
}

type MetaResponse struct {
	HTTPCode int `json:"http_code"`
	Limit    int `json:"limit,omitempty"`
	Offset   int `json:"offset,omitempty"`
	Total    int `json:"total,omitempty"`
}

type Response struct {
	Message string              `json:"message,omitempty"`
	Data    interface{}         `json:"data,omitempty"`
	Errors  []HttpResponseError `json:"errors,omitempty"`
	Meta    MetaResponse        `json:"meta"`
}

type InternalServerError struct {
	Message string
}

func (e InternalServerError) Error() string {
	return e.Message
}

func (e InternalServerError) ToHttpError() (int, []HttpResponseError) {
	return http.StatusInternalServerError, []HttpResponseError{
		{
			Message: e.Message,
		},
	}
}
