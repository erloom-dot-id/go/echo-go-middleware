package echogomiddleware

import (
	"encoding/json"
	"net/http"

	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
)

func WriteSuccessWithMessage(w http.ResponseWriter, data interface{}, message string, meta MetaResponse) {
	res := Response{
		Data:    data,
		Message: message,
		Meta:    meta,
	}

	w.WriteHeader(meta.HTTPCode)
	responseBody, _ := json.Marshal(res)
	_, _ = w.Write(responseBody)
}

func WriteSuccess(w http.ResponseWriter, data interface{}, meta MetaResponse) {
	res := Response{
		Data: data,
		Meta: meta,
	}

	w.WriteHeader(meta.HTTPCode)
	responseBody, _ := json.Marshal(res)
	_, _ = w.Write(responseBody)
}

func WriteError(w http.ResponseWriter, err error) {
	statusCode, httpErrors := translateError(err)
	meta := MetaResponse{
		HTTPCode: statusCode,
	}
	res := Response{
		Errors: httpErrors,
		Meta: MetaResponse{
			HTTPCode: statusCode,
		},
	}

	w.WriteHeader(meta.HTTPCode)
	responseBody, _ := json.Marshal(res)
	_, _ = w.Write(responseBody)
}

var utTranslator ut.Translator

func translateError(err error) (int, []HttpResponseError) {
	switch origErr := err.(type) {
	case HttpError:
		return origErr.ToHttpError()
	case validator.ValidationErrors:
		respErrors := make([]HttpResponseError, 0)
		for _, validationError := range origErr {
			respErrors = append(respErrors, HttpResponseError{
				Field:   validationError.Field(),
				Message: validationError.Translate(utTranslator),
			})
		}
		return http.StatusBadRequest, respErrors
	default:
		return InternalServerError{Message: err.Error()}.ToHttpError()
	}
}
